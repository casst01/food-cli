## Food CLI
Command Line tool for parsing food products from HTML

### Usage
From project root:

```sh
java -jar bin/food-cli.jar <Products Page URL>
```

Eg. Fetch Sainsburys Berries, Cherries, Currants:

```sh
java -jar bin/food-cli.jar https://jsainsburyplc.github.io/serverside-test/site/www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html
```

### Frameworks

- JSON Parsing: Gson
- Fetching and Parsing URLs: Jsoup
- CLI: Picocli

### Structure
Designed to allow easy addition of new `ProductFetchers` and `ProductMappers` to parse
Products from different HTML structures/sites

#### DocumentFetcher
Responsible for fetching the HTML content from a given URL.
Throws `DocumentFetchException` if HTML cannot be fetched or parsed.

#### ProductFetcher
Responsible for Fetching Products from given url. Interface defines 2 methods:

- `Product fetchProduct(String productUrl)`
    - Given a URL return a Product
    - Throws `DocumentFetchException` if HTML cannot be fetched or parsed
    - Throws `ProductMapException` if HTML cannot be mapped to Product

- `ProductList fetchProduct(String productPageUrl)`
    - Given a URL for a page containing a list of products, return a ProductList
    - Throws `MalformedUrlException` if URL for child product page could not be constructed
    - Throws `DocumentFetchException` if HTML cannot be fetched or parsed
    - Throws `ProductMapException` if HTML cannot be mapped to Product

#### ProductMapper
Responsible for Mapping from HTML to a Product POJO. Interface defines 1 method:

- `Product map(Document document)`
    - Given a HTML Document, return a Product
    - Throws `ProductMapException` if HTML cannot be mapped to Product
        - `ProductAttributeParseException` if an attribute could not be parsed (eg as Integer or Double)
        - `ProductRequiredAttributeMissingException` if a required attribute could be found in the HTML

#### ProductListFormatter
Returns a String representation of a ProductList. Interface provides 1 method:

- `String format(ProductList productList)`
    - Given a ProductList, return a String

#### Output Target
Represents destination to write output to. Interface contains 1 method:

- `void write(String content)`
    - Writes content to target

### Building
A build script is provided to build and deploy the current version into the `bin` directory.
This script calls `mvn clean package assembly:single` to package the app into a jar with dependencies.

### Testing
To execute the tests run `mvn clean test`

### Known Issues

The current implementation on master produces Json output, however Doubles are not padded to
2 decimal places (eg 1.5 -> 1.50). Possible fixes are being looked at in the `output-doubles-to-2-dp` branch.