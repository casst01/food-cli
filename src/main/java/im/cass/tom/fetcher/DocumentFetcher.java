package im.cass.tom.fetcher;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class DocumentFetcher {

    public Document fetchDocument(String url) throws DocumentFetchException {
        try {
            return Jsoup.connect(url).timeout(0).get();
        } catch (IOException | IllegalArgumentException e ) {
            throw new DocumentFetchException(String.format("Failed to fetch Document. %s", e.getMessage()));
        }
    }

}
