package im.cass.tom.fetcher;

import im.cass.tom.mapper.ProductMapException;
import im.cass.tom.model.Product;
import im.cass.tom.model.ProductList;

import java.net.MalformedURLException;

public interface ProductFetcher {

    Product fetchProduct(String productUrl) throws DocumentFetchException, ProductMapException;

    ProductList fetchProducts(String productUrl) throws DocumentFetchException, MalformedURLException, ProductMapException;

}
