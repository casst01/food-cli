package im.cass.tom.fetcher;

import im.cass.tom.model.Product;
import im.cass.tom.mapper.ProductMapException;
import im.cass.tom.mapper.SainsburysProductMapper;
import im.cass.tom.model.ProductList;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class SainsburysProductFetcher implements ProductFetcher {

    DocumentFetcher documentFetcher;

    public SainsburysProductFetcher(DocumentFetcher documentFetcher) {
        this.documentFetcher = documentFetcher;
    }

    public Product fetchProduct(String productUrl) throws DocumentFetchException, ProductMapException {
        SainsburysProductMapper productMapper = new SainsburysProductMapper();
        Document productPage = documentFetcher.fetchDocument(productUrl);
        return productMapper.map(productPage);
    }

    public ProductList fetchProducts(String productsUrl) throws DocumentFetchException, MalformedURLException, ProductMapException {
        List<Product> products = new ArrayList<>();
        Document productsPage = documentFetcher.fetchDocument(productsUrl);

        Elements productLinks = productsPage.select("ul.productLister li.gridItem div.productNameAndPromotions a");
        for (Element productLink : productLinks) {
            String productUrl = new URL(new URL(productsUrl), productLink.attr("href")).toString();
            products.add(fetchProduct(productUrl));
        }

        return new ProductList(products);
    }
}
