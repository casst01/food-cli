package im.cass.tom.fetcher;

public class DocumentFetchException extends Exception {
    public DocumentFetchException(String msg) {
        super(msg);
    }
}
