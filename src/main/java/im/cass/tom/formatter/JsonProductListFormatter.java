package im.cass.tom.formatter;

import com.google.gson.*;
import com.google.gson.annotations.SerializedName;
import im.cass.tom.model.Product;
import im.cass.tom.model.ProductList;

import java.util.List;
import java.util.stream.Collectors;

public class JsonProductListFormatter implements ProductListFormatter {

    private final Gson gson;

    public JsonProductListFormatter() {
        this.gson = new GsonBuilder()
                .setPrettyPrinting()
                .disableHtmlEscaping()
                .create();
    }

    @Override
    public String format(ProductList productList) {
        return gson.toJson(ProductListDTO.fromProductList(productList));
    }

}

class ProductDTO {

    @SerializedName("title")
    private String title;

    @SerializedName("description")
    private String description;

    @SerializedName("unit_price")
    private Double unitPrice;

    @SerializedName("kcal_per_100g")
    private Integer kcalPer100g;

    public static ProductDTO fromProduct(Product product) {
        return new ProductDTO(product);
    }

    public ProductDTO(Product product) {
        this.title = product.getTitle();
        this.description = product.getDescription();
        this.unitPrice = product.getUnitPrice();
        this.kcalPer100g = product.getKcalPer100g();
    }
}

class ProductListDTO {

    @SerializedName("products")
    private final List<ProductDTO> products;

    @SerializedName("total")
    private final Double total;

    public static ProductListDTO fromProductList(ProductList productList) {
        return new ProductListDTO(productList);
    }

    public ProductListDTO(ProductList productList) {
        this.products = productList.getProducts().stream().map(ProductDTO::fromProduct).collect(Collectors.toList());
        this.total = productList.getTotal();
    }
}
