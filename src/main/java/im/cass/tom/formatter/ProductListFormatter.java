package im.cass.tom.formatter;

import im.cass.tom.model.ProductList;

public interface ProductListFormatter {
    String format(ProductList productList);
}
