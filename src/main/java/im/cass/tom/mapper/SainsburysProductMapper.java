package im.cass.tom.mapper;

import im.cass.tom.model.Product;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SainsburysProductMapper implements ProductMapper {

    public Product map(Document document) throws ProductMapException {

        String title = extractRequiredElement(document, ".productTitleDescriptionContainer h1");
        String description = extractRequiredElement(document, ".productText p");
        Double unitPrice = extractDouble(extractRequiredElement(document, ".pricePerUnit").replaceAll("£", ""));

        Element caloriesElement = document.selectFirst(".nutritionTable tr.tableRow0 td");
        Integer calories = null;
        if(caloriesElement != null) {
            calories = extractInteger(caloriesElement.ownText().replace("kcal", ""));
        }

        return new Product(title, description, unitPrice, calories);
    }

    private String extractRequiredElement(Document document, String elementPath) throws ProductMapException {
        Element element = document.selectFirst(elementPath);
        if (element == null) {
            throw new ProductRequiredAttributeMissingException(String.format("Could not locate element at path  `%s`.", elementPath));
        }
        return element.ownText();
    }

    private Double extractDouble(String value) throws ProductAttributeParseException {
        try {
            return Double.parseDouble(value);
        }
        catch( NumberFormatException e) {
            throw new ProductAttributeParseException(String.format("Failed to parse `%s` as Double.", value));
        }
    }

    private Integer extractInteger(String value) throws ProductAttributeParseException {
        try {
            return Integer.parseInt(value);
        }
        catch( NumberFormatException e) {
            throw new ProductAttributeParseException(String.format("Failed to parse `%s` as Integer.", value));
        }
    }

}
