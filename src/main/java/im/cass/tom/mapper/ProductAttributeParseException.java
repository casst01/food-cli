package im.cass.tom.mapper;

public class ProductAttributeParseException extends ProductMapException {
    public ProductAttributeParseException(String message) {
        super(message);
    }
}
