package im.cass.tom.mapper;

import im.cass.tom.model.Product;
import org.jsoup.nodes.Document;

public interface ProductMapper {

    Product map(Document document) throws ProductMapException;

}
