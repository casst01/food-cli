package im.cass.tom.mapper;

public class ProductRequiredAttributeMissingException extends ProductMapException {
    public ProductRequiredAttributeMissingException(String message) {
        super(message);
    }
}
