package im.cass.tom.mapper;

public class ProductMapException extends Exception {
    public ProductMapException(String message) {
        super(message);
    }
}
