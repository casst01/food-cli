package im.cass.tom;

import im.cass.tom.command.FetchProductsCommand;
import im.cass.tom.formatter.JsonProductListFormatter;
import im.cass.tom.fetcher.DocumentFetcher;
import im.cass.tom.fetcher.SainsburysProductFetcher;
import picocli.CommandLine;

public class App
{
    public static void main( String[] args )
    {
        FetchProductsCommand cmd = new FetchProductsCommand(
                new SainsburysProductFetcher(new DocumentFetcher()),
                new JsonProductListFormatter(),
                System.out::println
        );

        CommandLine.run(cmd, args);
    }

}
