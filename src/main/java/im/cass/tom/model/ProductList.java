package im.cass.tom.model;

import java.util.List;

public class ProductList {
    private final List<Product> products;
    private final Double total;

    public ProductList(List<Product> products) {
        this.products = products;
        this.total = calculateTotal(products);
    }

    private Double calculateTotal(List<Product> products) {
        Double total = 0d;
        for (Product product : products) {
            total += product.getUnitPrice();
        }
        return total;
    }

    public List<Product> getProducts() {
        return products;
    }

    public Double getTotal() {
        return total;
    }

    @Override
    public String toString() {
        return "ProductList{" +
                "products=" + products +
                ", total=" + total +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductList that = (ProductList) o;

        if (!getProducts().equals(that.getProducts())) return false;
        return getTotal().equals(that.getTotal());

    }

    @Override
    public int hashCode() {
        int result = getProducts().hashCode();
        result = 31 * result + getTotal().hashCode();
        return result;
    }
}
