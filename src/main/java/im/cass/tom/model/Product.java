package im.cass.tom.model;

public class Product {

    private String title;
    private String description;
    private Double unitPrice;
    private Integer kcalPer100g;

    public Product(String title, String description, double unitPrice, Integer kcalPer100g) {
        this.title = title;
        this.description = description;
        this.unitPrice = unitPrice;
        this.kcalPer100g = kcalPer100g;
    }

    public String getTitle() {
        return this.title;
    }

    public String getDescription() {
        return description;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public Integer getKcalPer100g() {
        return kcalPer100g;
    }

    @Override
    public String toString() {
        return "Product{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", unitPrice=" + unitPrice +
                ", kcalPer100g=" + kcalPer100g +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (getKcalPer100g() != product.getKcalPer100g()) return false;
        if (!getTitle().equals(product.getTitle())) return false;
        if (!getDescription().equals(product.getDescription())) return false;
        return getUnitPrice().equals(product.getUnitPrice());

    }

    @Override
    public int hashCode() {
        int result = getTitle().hashCode();
        result = 31 * result + getDescription().hashCode();
        result = 31 * result + getUnitPrice().hashCode();
        result = 31 * result + getKcalPer100g();
        return result;
    }
}
