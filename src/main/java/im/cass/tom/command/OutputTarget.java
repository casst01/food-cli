package im.cass.tom.command;

public interface OutputTarget {
    void write(String content);
}
