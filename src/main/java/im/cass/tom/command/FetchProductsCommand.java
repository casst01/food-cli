package im.cass.tom.command;

import im.cass.tom.fetcher.DocumentFetchException;
import im.cass.tom.fetcher.SainsburysProductFetcher;
import im.cass.tom.formatter.ProductListFormatter;
import im.cass.tom.mapper.ProductMapException;
import im.cass.tom.model.ProductList;
import picocli.CommandLine;

import java.net.MalformedURLException;

@CommandLine.Command(name = "fetch-products")
public class FetchProductsCommand implements Runnable {

    @CommandLine.Parameters(description = "URL of products page")
    private String productsPageUrl;

    private final SainsburysProductFetcher productFetcher;
    private final ProductListFormatter outputFormatter;
    private final OutputTarget outputTarget;

    public FetchProductsCommand(SainsburysProductFetcher productFetcher, ProductListFormatter outputFormatter, OutputTarget outputTarget) {
        this.productFetcher = productFetcher;
        this.outputFormatter = outputFormatter;
        this.outputTarget = outputTarget;
    }

    @Override
    public void run() {

        try {
            ProductList productList = this.productFetcher.fetchProducts(productsPageUrl);
            outputTarget.write(outputFormatter.format(productList));

        } catch (DocumentFetchException | ProductMapException | MalformedURLException e) {
            System.out.println(e.getMessage());
        }

    }

    public void setProductsPageUrl(String productsPageUrl) {
        this.productsPageUrl = productsPageUrl;
    }
}
