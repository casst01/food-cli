package im.cass.tom.fetcher;

import im.cass.tom.mapper.ProductMapException;
import im.cass.tom.model.Product;
import im.cass.tom.model.ProductList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SainsburysProductFetcherTest {

    private SainsburysProductFetcher productFetcher;

    @Mock
    DocumentFetcher documentFetcher;

    @Before
    public void setup() throws IOException, URISyntaxException {
        productFetcher = new SainsburysProductFetcher(documentFetcher);
    }

    @Test
    public void testFetchProduct_shouldReturnProduct() throws DocumentFetchException, URISyntaxException, IOException, ProductMapException {
        String productUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html";
        when(documentFetcher.fetchDocument(productUrl)).thenReturn(parseFileAtPath("/html/mixed-berries.html"));

        Product product = productFetcher.fetchProduct(productUrl);

        assertEquals("Sainsbury's Mixed Berries 300g", product.getTitle());
        assertEquals("by Sainsbury's mixed berries", product.getDescription());
        assertEquals(Double.valueOf(3.50), product.getUnitPrice());
    }

    @Test(expected = DocumentFetchException.class)
    public void testFetchProduct_throwsDocumentFetchException_whenExceptionDuringFetchDocument() throws IOException, DocumentFetchException, ProductMapException {
        String productUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html";
        when(documentFetcher.fetchDocument(productUrl)).thenThrow(new DocumentFetchException("Error Fetching Document"));
        productFetcher.fetchProduct(productUrl);
    }

    @Test
    public void testFetchProducts_shouldReturnProductList() throws IOException, URISyntaxException, DocumentFetchException, ProductMapException {
        String productsPageUrl = "https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
        String blueberriesUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html";
        String mixedBerriesUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html";
        String mixedBerriesTwinPackUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berry-twin-pack-200g-7696255-p-44.html";

        when(documentFetcher.fetchDocument(productsPageUrl)).thenReturn(parseFileAtPath("/html/berries-cherries-currants-products.html"));
        when(documentFetcher.fetchDocument(blueberriesUrl)).thenReturn(parseFileAtPath("/html/blueberries-with-kcal.html"));
        when(documentFetcher.fetchDocument(mixedBerriesUrl)).thenReturn(parseFileAtPath("/html/mixed-berries.html"));
        when(documentFetcher.fetchDocument(mixedBerriesTwinPackUrl)).thenReturn(parseFileAtPath("/html/mixed-berries-twin-pack-with-multiline-description.html"));

        ProductList productList = productFetcher.fetchProducts(productsPageUrl);

        assertEquals(3, productList.getProducts().size());
        assertEquals("Sainsbury's Blueberries 200g", productList.getProducts().get(0).getTitle());
        assertEquals("Sainsbury's Mixed Berries 300g", productList.getProducts().get(1).getTitle());
        assertEquals("Sainsbury's Mixed Berry Twin Pack 200g", productList.getProducts().get(2).getTitle());
    }

    @Test(expected = DocumentFetchException.class)
    public void testFetchProducts_throwsDocumentFetchException_whenExceptionDuringFetchingDocument() throws DocumentFetchException, IOException, URISyntaxException, ProductMapException {
        String productsPageUrl = "https://www.sainsburys.co.uk/webapp/wcs/stores/servlet/gb/groceries/berries-cherries-currants6039.html";
        String blueberriesUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-blueberries-200g.html";
        String mixedBerriesUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berries-300g.html";
        String mixedBerriesTwinPackUrl = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants/sainsburys-mixed-berry-twin-pack-200g-7696255-p-44.html";

        when(documentFetcher.fetchDocument(productsPageUrl)).thenReturn(parseFileAtPath("/html/berries-cherries-currants-products.html"));
        when(documentFetcher.fetchDocument(blueberriesUrl)).thenReturn(parseFileAtPath("/html/blueberries-with-kcal.html"));
        when(documentFetcher.fetchDocument(mixedBerriesUrl)).thenReturn(parseFileAtPath("/html/mixed-berries.html"));
        when(documentFetcher.fetchDocument(mixedBerriesTwinPackUrl)).thenThrow(new DocumentFetchException("Error Fetching Document"));

        productFetcher.fetchProducts(productsPageUrl);
    }

    private Document parseFileAtPath(String path) throws IOException, URISyntaxException {
        URL url = this.getClass().getResource(path);
        File file = new File(url.toURI());

        return Jsoup.parse(file, String.valueOf(Charset.defaultCharset()));
    }

}
