package im.cass.tom.command;

import im.cass.tom.fetcher.DocumentFetchException;
import im.cass.tom.fetcher.SainsburysProductFetcher;
import im.cass.tom.formatter.ProductListFormatter;
import im.cass.tom.mapper.ProductMapException;
import im.cass.tom.model.Product;
import im.cass.tom.model.ProductList;
import org.junit.Test;

import java.net.MalformedURLException;
import java.util.Arrays;

import static org.mockito.Mockito.*;

public class FetchProductsCommandTest {

    @Test
    public void testFetchProducts_writesProductListToGivenOutputTarget() throws DocumentFetchException, ProductMapException, MalformedURLException {

        String url = "https://www.sainsburys.co.uk/shop/gb/groceries/berries-cherries-currants";
        ProductListFormatter outputFormatter = mock(ProductListFormatter.class);
        SainsburysProductFetcher productFetcher = mock(SainsburysProductFetcher.class);
        OutputTarget outputTarget = mock(OutputTarget.class);

        FetchProductsCommand cmd = new FetchProductsCommand(productFetcher, outputFormatter, outputTarget);
        cmd.setProductsPageUrl(url);

        ProductList productList = new ProductList(Arrays.asList(
            new Product("Apples", "Granny Smith", 1.00, 0),
            new Product("Bananas", "Organic Bananas", 2.75, 0),
            new Product("Cucumber", "", 1.25, 0)
        ));

        when(productFetcher.fetchProducts(url)).thenReturn(productList);
        when(outputFormatter.format(productList)).thenReturn("Product List");

        cmd.run();

        verify(outputFormatter).format(any(ProductList.class));
        verify(outputTarget, times(1)).write("Product List");
    }

}
