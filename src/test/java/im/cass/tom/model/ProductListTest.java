package im.cass.tom.model;


import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ProductListTest {

    @Test
    public void testProductSummary_calculatesTotalPriceOfProducts() {

        List<Product> products = Arrays.asList(
            new Product("Apples", "Granny Smith", 1.00, 0),
            new Product("Bananas", "Organic Bananas", 2.75, 0),
            new Product("Cucumber", "", 1.25, 0)
        );

        ProductList productList = new ProductList(products);

        assertEquals(Double.valueOf(5.00), productList.getTotal());
    }
}
