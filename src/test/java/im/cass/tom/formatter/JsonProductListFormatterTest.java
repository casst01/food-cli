package im.cass.tom.formatter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import im.cass.tom.model.Product;
import im.cass.tom.model.ProductList;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class JsonProductListFormatterTest {

    private Gson gson;
    private JsonProductListFormatter formatter;

    @Before
    public void setup() {
        gson = new Gson();
        formatter = new JsonProductListFormatter();
    }

    @Test
    public void testFormat_returnsJsonWithSnakeCasedProperties() {
        ProductList productList = new ProductList(Arrays.asList(
                new Product("Sainsbury's Apples", "Granny Smith", 1.00, 54),
                new Product("Sainsbury's Bananas", "Organic Bananas", 2.75, 34),
                new Product("Sainsbury's Cucumber", "", 1.25, 23)
        ));

        String productListJson = formatter.format(productList);
        JsonObject json = gson.fromJson(productListJson, JsonObject.class);
        JsonArray products = json.getAsJsonArray("products");

        assertEquals(3, products.size());
        assertEquals(productList.getTotal(), Double.valueOf(json.get("total").getAsDouble()));

        assertProductEqualToJson(productList.getProducts().get(0), products.get(0));
        assertProductEqualToJson(productList.getProducts().get(1), products.get(1));
        assertProductEqualToJson(productList.getProducts().get(2), products.get(2));
    }

    @Test
    public void testFormat_doesNotIncludeKcal_whenKcalNull() {
        ProductList productList = new ProductList(Arrays.asList(
                new Product("Sainsbury's Apples", "Granny Smith", 1.00, 35),
                new Product("Sainsbury's Bananas", "Organic Bananas", 2.75, null),
                new Product("Sainsbury's Cucumber", "", 1.25, 54)
        ));

        String productListJson = formatter.format(productList);
        JsonArray products = gson.fromJson(productListJson, JsonObject.class).getAsJsonArray("products");

        assertNull(products.get(1).getAsJsonObject().get("kcal_per_100g"));
    }

    private void assertProductEqualToJson(Product product, JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        assertEquals(product.getTitle(), jsonObject.get("title").getAsString());
        assertEquals(product.getDescription(), jsonObject.get("description").getAsString());
        assertEquals(product.getUnitPrice(), Double.valueOf(jsonObject.get("unit_price").getAsDouble()));
        assertEquals(product.getKcalPer100g(), Integer.valueOf(jsonObject.get("kcal_per_100g").getAsInt()));
    }
}
