package im.cass.tom.mapper;

import im.cass.tom.model.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;

import static org.junit.Assert.assertEquals;

public class SainsburysProductMapperTest {

    private SainsburysProductMapper productMapper;

    @Before
    public void setup() {
        productMapper = new SainsburysProductMapper();
    }

    @Test
    public void testMap_shouldCreateProduct() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/mixed-berries.html");

        Product product = productMapper.map(doc);

        assertEquals("Sainsbury's Mixed Berries 300g", product.getTitle());
        assertEquals("by Sainsbury's mixed berries", product.getDescription());
        assertEquals(Double.valueOf(3.50), product.getUnitPrice());
    }

    @Test
    public void testMap_shouldSetCaleriesPer100G_whenAvailable() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/blueberries-with-kcal.html");

        Product product = productMapper.map(doc);

        assertEquals("Sainsbury's Blueberries 200g", product.getTitle());
        assertEquals("by Sainsbury's blueberries", product.getDescription());
        assertEquals(Double.valueOf(1.75), product.getUnitPrice());
        assertEquals(Integer.valueOf(45), product.getKcalPer100g());
    }

    @Test
    public void testMap_shouldSetSingleLineDescription_whenMultipleLinesAvailable() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/mixed-berries-twin-pack-with-multiline-description.html");

        Product product = productMapper.map(doc);

        assertEquals("Sainsbury's Mixed Berry Twin Pack 200g", product.getTitle());
        assertEquals("Mixed Berries", product.getDescription());
        assertEquals(Double.valueOf(2.75), product.getUnitPrice());
    }

    @Test(expected = ProductRequiredAttributeMissingException.class)
    public void testMap_throwsProductMapException_whenRequiredAttributeNotFound() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/mixed-berries-missing-name.html");
        productMapper.map(doc);
    }

    @Test(expected = ProductAttributeParseException.class)
    public void testMap_throwsProductMapException_whenAttributeIsInvalid() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/blueberries-with-invalid-kcal.html");
        productMapper.map(doc);
    }

    @Test(expected = ProductAttributeParseException.class)
    public void testMap_throwsProductMapException_whenRequiredAttributeIsInvalid() throws IOException, URISyntaxException, ProductMapException {
        Document doc = parseFileAtPath("/html/blueberries-with-kcal-and-invalid-price.html");
        productMapper.map(doc);
    }

    private Document parseFileAtPath(String path) throws IOException, URISyntaxException {
        URL url = this.getClass().getResource(path);
        File file = new File(url.toURI());

        return Jsoup.parse(file, String.valueOf(Charset.defaultCharset()));
    }
}
