#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

mvn clean package assembly:single
rm $DIR/food-cli.jar
mv target/food-cli-*-jar-with-dependencies.jar $DIR/food-cli.jar
